package com.rivaldy.id.gdsc.ui.detail

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.rivaldy.id.gdsc.R
import com.rivaldy.id.gdsc.data.model.EventData
import com.rivaldy.id.gdsc.databinding.ActivityEventDetailBinding
import com.rivaldy.id.gdsc.ui.ViewPagerAdapter

class EventDetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityEventDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEventDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initData()
        initView()
    }

    private fun initData() {
        extraEvent = intent.getParcelableExtra(EXTRA_EVENT)
        if (extraEvent != null) {
            binding.circleImageView.setImageResource(extraEvent?.imageId ?: R.color.white)
            binding.titleTV.text = extraEvent?.title
            binding.locationTV.text = extraEvent?.location
        }
    }

    private fun initView() {
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.detail)
        binding.toolbar.setNavigationOnClickListener { finish() }

        val titles = arrayListOf(getString(R.string.description), getString(R.string.about))
        val fragments: ArrayList<Fragment> = arrayListOf(EventDetailDescFragment(), EventDetailAboutFragment())
        val viewPagerAdapter = ViewPagerAdapter(titles, fragments, supportFragmentManager)
        binding.viewPager.adapter = viewPagerAdapter
        binding.tabLayout.setupWithViewPager(binding.viewPager)
    }

    companion object {
        const val EXTRA_EVENT = "extra_event"
        var extraEvent: EventData? = null
    }
}