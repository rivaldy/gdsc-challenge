package com.rivaldy.id.gdsc.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.rivaldy.id.gdsc.data.model.EventData
import com.rivaldy.id.gdsc.databinding.RowItemEventBinding

class EventAdapter(
    private val listener: (EventData) -> Unit
) : ListAdapter<EventData, EventAdapter.ViewHolder>(DIFF_CALLBACK) {

    inner class ViewHolder(private val binding: RowItemEventBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bindItem(item: EventData) {
            binding.typeTV.text = item.type
            binding.titleTV.text = item.title
            binding.descriptionTV.text = item.description
            binding.root.setOnClickListener { listener(item) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowItemEventBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(getItem(position))
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<EventData>() {
            override fun areItemsTheSame(oldItem: EventData, newItem: EventData): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: EventData, newItem: EventData): Boolean {
                return oldItem == newItem
            }
        }
    }
}