package com.rivaldy.id.gdsc.ui.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.rivaldy.id.gdsc.data.model.EventData
import com.rivaldy.id.gdsc.databinding.FragmentEventDetailDescBinding

class EventDetailDescFragment : Fragment() {
    private var extraEvent: EventData? = null
    private lateinit var binding: FragmentEventDetailDescBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentEventDetailDescBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView() {
        extraEvent = EventDetailActivity.extraEvent
        if (extraEvent != null) {
            binding.bodyTV.text = extraEvent?.description
        }
    }
}