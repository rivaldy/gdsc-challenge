package com.rivaldy.id.gdsc.data.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class EventData(
    val id: Int? = 0,
    val type: String? = "",
    val title: String? = "",
    val description: String? = "",
    val about: String? = "",
    val location: String? = "",
    val imageId: Int? = 0,
) : Parcelable