package com.rivaldy.id.gdsc.ui

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import androidx.appcompat.app.AppCompatActivity
import com.rivaldy.id.gdsc.R
import com.rivaldy.id.gdsc.data.model.EventData
import com.rivaldy.id.gdsc.databinding.ActivityMainBinding
import com.rivaldy.id.gdsc.ui.detail.EventDetailActivity

class MainActivity : AppCompatActivity() {

    private val eventAdapter by lazy { EventAdapter { eventClick(it) } }
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initView()
        initData()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    private fun initView() {
        setSupportActionBar(binding.appBar.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        supportActionBar?.title = getString(R.string.list_event)
        binding.listDataRV.adapter = eventAdapter
    }

    private fun initData() {
        val events = mutableListOf<EventData>()
        for (i in 1..20) {
            val type = getString(R.string.str_event_type, i)
            val title = getString(R.string.str_event_title, i)
            val description = getString(R.string.lorem_sample)
            val about = getString(R.string.lorem_sample)
            val location = getString(R.string.str_event_location, i)
            val imageId = R.drawable.gdsc_unhas
            events.add(EventData(i, type, title, description, about, location, imageId))
        }
        eventAdapter.submitList(events)
    }

    private fun eventClick(data: EventData) {
        val intent = Intent(this, EventDetailActivity::class.java)
        intent.putExtra(EventDetailActivity.EXTRA_EVENT, data)
        startActivity(intent)
    }
}